package ru.inshakov.tm.controller;

import ru.inshakov.tm.api.ICommandController;
import ru.inshakov.tm.api.ICommandService;
import ru.inshakov.tm.model.Command;
import ru.inshakov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Vsevolod Inshakov");
        System.out.println("E-MAIL: vinshakov@tsconsulting.com");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command.getName());
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String arg = command.getArg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + NumberUtil.format(availableProcessors));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory (bytes): " + NumberUtil.format(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = (isMaxMemory ? "no limit" : NumberUtil.format(maxMemory));
        System.out.println("Maximum memory (bytes): " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM (bytes): " + NumberUtil.format(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM (bytes): " + NumberUtil.format(usedMemory));
        System.out.println("[OK]");
    }

}
