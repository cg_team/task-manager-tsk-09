package ru.inshakov.tm.service;

import ru.inshakov.tm.api.ICommandRepository;
import ru.inshakov.tm.model.Command;
import ru.inshakov.tm.api.ICommandService;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
